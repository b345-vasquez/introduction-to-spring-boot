package com.zuitt.wdc044.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id // Primary Key
    @GeneratedValue // Auto Increment
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user") // One-to-Many Relationship
    @JsonIgnore
    public Set<Post> post;


    public User(){}

    public User(String username, String password){
        this.username=username;
        this.password=password;
    }


    public Set<Post> getPost() {
        return this.post;
    }

    public String getUsername(){
        return this.username;
    }
    public  void setUsername(){
        this.username=username;
    }

    public String getPassword(){
        return this.password;
    }
    public  void setPassword(){
        this.password = password;
    }

    public Long getId() {
        return id;
    }
}
