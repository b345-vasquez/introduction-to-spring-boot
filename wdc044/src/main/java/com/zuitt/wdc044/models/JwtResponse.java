package com.zuitt.wdc044.models;

import java.io.Serializable;
public class JwtResponse implements Serializable{
    private static final long serialVersionUID = 1234567890L;
    private final String jwtToken;

    public JwtResponse(String jwtToken){
        this.jwtToken = jwtToken;
    }

    public String getJwtToken(){
        return this.jwtToken;
    }
}
