package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface PostService {

//    CREATE POST METHOD
    void createPost(String stringToken, Post post);

//    GET POST METHOD
    Iterable<Post> getPosts();

//    UPDATE POST METHOD
    ResponseEntity<?> updatePost(Long id, String stringToken, Post post);

//    DELETING
    ResponseEntity<?> deletePost(Long id, String stringToken);

//    Getting user post
    Iterable<Post> getMyPosts(String stringToken);
//Iterable<Post> getMyPost(String stringToken);

}
